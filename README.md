texto-con-fondos-animados-y-gradientes
======================================

archivos con el codigo fuente del tutorial para crear texto con imágenes o gradientes de fondo

este repositorio contiene dos archivos html y dos archivos css, como ejemplos.

Contenido:

- el archivo "imagen.html" es el del ejemplo para colocar una imagen como fondo de un texto, y su archivo .css es "estilo.css" 
- el archivo "gradiente.html" es el del ejemplo para colocar un gradiente como fondo de un texto, y su css enlazado es "estilo2.css"
- imagenes usadas para los ejemplos.
